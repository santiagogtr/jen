package test.java.gf.my_test;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;
import main.java.gf.my_test.BookInterface;

import org.junit.Test;
import org.mockito.Mockito;

public class BookMockTest {
	
	

	@Test
	public void test1()  {
	  //  create mock
	//I only use interface
	  BookInterface test = Mockito.mock(BookInterface.class);
	  
	  // define return value for method scrollForward()
	  when(test.scrollForward()).thenReturn(false);
	  
	  // use mock in test.... 
	  assertFalse(test.scrollForward());
	}

}
